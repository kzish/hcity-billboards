﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Leasestatuses
    {
        public Leasestatuses()
        {
            Lease = new HashSet<Lease>();
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Lease> Lease { get; set; }
    }
}
