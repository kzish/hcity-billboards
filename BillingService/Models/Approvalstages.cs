﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Approvalstages
    {
        public int Id { get; set; }
        public int? AssetStatusId { get; set; }
        public int? RoleId { get; set; }

        public virtual Assetstatuses AssetStatus { get; set; }
        public virtual Roles Role { get; set; }
    }
}
