﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Users
    {
        public Users()
        {
            Approvals = new HashSet<Approvals>();
            Assetpayments = new HashSet<Assetpayments>();
            Assets = new HashSet<Assets>();
            Notifications = new HashSet<Notifications>();
            TransactionsApprovedByUser = new HashSet<Transactions>();
            TransactionsUser = new HashSet<Transactions>();
        }

        public int Id { get; set; }
        public int? CompanyId { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public byte[] Email { get; set; }
        public string Phone { get; set; }
        public int? RoleId { get; set; }
        public string IdentityUserId { get; set; }

        public virtual Companies Company { get; set; }
        public virtual Aspnetusers IdentityUser { get; set; }
        public virtual Roles Role { get; set; }
        public virtual ICollection<Approvals> Approvals { get; set; }
        public virtual ICollection<Assetpayments> Assetpayments { get; set; }
        public virtual ICollection<Assets> Assets { get; set; }
        public virtual ICollection<Notifications> Notifications { get; set; }
        public virtual ICollection<Transactions> TransactionsApprovedByUser { get; set; }
        public virtual ICollection<Transactions> TransactionsUser { get; set; }
    }
}
