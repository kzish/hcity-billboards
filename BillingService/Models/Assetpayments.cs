﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Assetpayments
    {
        public int Id { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int? UserId { get; set; }
        public int? FeesId { get; set; }
        public int? AssetId { get; set; }
        public decimal? Amount { get; set; }
        public int? PaymentMethod { get; set; }
        public sbyte IsPayment { get; set; }
        public int? TransactionId { get; set; }

        public virtual Assets Asset { get; set; }
        public virtual Fees Fees { get; set; }
        public virtual Users User { get; set; }
    }
}
