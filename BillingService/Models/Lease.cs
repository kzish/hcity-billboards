﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Lease
    {
        public Lease()
        {
            Assets = new HashSet<Assets>();
        }

        public int Id { get; set; }
        public int? AssetId { get; set; }
        public string Name { get; set; }
        public sbyte? IsPenaltyFees { get; set; }
        public int? StatusId { get; set; }
        public DateTime? CommencementDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? LeaseTypeId { get; set; }
        public decimal Amount { get; set; }

        public virtual Assets Asset { get; set; }
        public virtual Leasetypes LeaseType { get; set; }
        public virtual Leasestatuses Status { get; set; }
        public virtual ICollection<Assets> Assets { get; set; }
    }
}
