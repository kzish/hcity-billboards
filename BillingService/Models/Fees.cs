﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Fees
    {
        public Fees()
        {
            Assetpayments = new HashSet<Assetpayments>();
            Transactions = new HashSet<Transactions>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public decimal? Amount { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Assetpayments> Assetpayments { get; set; }
        public virtual ICollection<Transactions> Transactions { get; set; }
    }
}
