﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Assets
    {
        public Assets()
        {
            Approvals = new HashSet<Approvals>();
            Assetpayments = new HashSet<Assetpayments>();
            LeaseNavigation = new HashSet<Lease>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public int? AssetTypeId { get; set; }
        public int? CompanyId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int? ContactUserId { get; set; }
        public decimal Balance { get; set; }
        public decimal Used { get; set; }
        public int? PaymentStatusId { get; set; }
        public int? LeaseId { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public string LocationDescription { get; set; }
        public int? RoadId { get; set; }
        public int? StatusId { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public decimal? Height { get; set; }
        public int? Type { get; set; }
        public int? SchedulerLastModifiedMonthNumber { get; set; }

        public virtual Assettypes AssetType { get; set; }
        public virtual Companies Company { get; set; }
        public virtual Users ContactUser { get; set; }
        public virtual Lease Lease { get; set; }
        public virtual Paymentstatuses PaymentStatus { get; set; }
        public virtual Roads Road { get; set; }
        public virtual Assetstatuses Status { get; set; }
        public virtual ICollection<Approvals> Approvals { get; set; }
        public virtual ICollection<Assetpayments> Assetpayments { get; set; }
        public virtual ICollection<Lease> LeaseNavigation { get; set; }
    }
}
