﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class AdminEmails
    {
        public int Id { get; set; }
        public string Email { get; set; }
    }
}
