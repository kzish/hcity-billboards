﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Transactions
    {
        public int Id { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int? UserId { get; set; }
        public int? FeesId { get; set; }
        public decimal? Amount { get; set; }
        public int PaymentMethod { get; set; }
        public sbyte IsPayment { get; set; }
        public bool IsRejected { get; set; }
        public string Filename { get; set; }
        public sbyte IsApproved { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public int? ApprovedByUserId { get; set; }
        public string ClientEmail { get; set; }
        public int? AssetId { get; set; }

        public virtual Users ApprovedByUser { get; set; }
        public virtual Fees Fees { get; set; }
        public virtual Users User { get; set; }
    }
}
