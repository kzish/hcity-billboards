﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Assettypes
    {
        public Assettypes()
        {
            Assets = new HashSet<Assets>();
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Assets> Assets { get; set; }
    }
}
