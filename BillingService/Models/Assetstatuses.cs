﻿using System;
using System.Collections.Generic;

namespace BillingService.Models
{
    public partial class Assetstatuses
    {
        public Assetstatuses()
        {
            Approvals = new HashSet<Approvals>();
            Approvalstages = new HashSet<Approvalstages>();
            Assets = new HashSet<Assets>();
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Approvals> Approvals { get; set; }
        public virtual ICollection<Approvalstages> Approvalstages { get; set; }
        public virtual ICollection<Assets> Assets { get; set; }
    }
}
