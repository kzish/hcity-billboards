﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BillingService.Models
{
    public partial class dbContext : DbContext
    {
        public dbContext()
        {
        }

        public dbContext(DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AdminEmails> AdminEmails { get; set; }
        public virtual DbSet<Approvals> Approvals { get; set; }
        public virtual DbSet<Approvalstages> Approvalstages { get; set; }
        public virtual DbSet<Aspnetroles> Aspnetroles { get; set; }
        public virtual DbSet<Aspnetuserclaims> Aspnetuserclaims { get; set; }
        public virtual DbSet<Aspnetuserlogins> Aspnetuserlogins { get; set; }
        public virtual DbSet<Aspnetuserroles> Aspnetuserroles { get; set; }
        public virtual DbSet<Aspnetusers> Aspnetusers { get; set; }
        public virtual DbSet<Assetpayments> Assetpayments { get; set; }
        public virtual DbSet<Assets> Assets { get; set; }
        public virtual DbSet<Assetstatuses> Assetstatuses { get; set; }
        public virtual DbSet<Assettypes> Assettypes { get; set; }
        public virtual DbSet<Companies> Companies { get; set; }
        public virtual DbSet<Fees> Fees { get; set; }
        public virtual DbSet<Lease> Lease { get; set; }
        public virtual DbSet<Leasestatuses> Leasestatuses { get; set; }
        public virtual DbSet<Leasetypes> Leasetypes { get; set; }
        public virtual DbSet<Notifications> Notifications { get; set; }
        public virtual DbSet<Paymentstatuses> Paymentstatuses { get; set; }
        public virtual DbSet<Roads> Roads { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("Server=localhost;Database=hcity;User=root;Password=;TreatTinyAsBoolean=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdminEmails>(entity =>
            {
                entity.ToTable("admin_emails");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<Approvals>(entity =>
            {
                entity.ToTable("approvals");

                entity.HasIndex(e => e.AssetStatusId)
                    .HasName("approvalsfk3");

                entity.HasIndex(e => e.AssetsId)
                    .HasName("approvalsfk1");

                entity.HasIndex(e => e.UserId)
                    .HasName("approvalsfk2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ApprovalDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.AssetStatusId).HasColumnType("int(11)");

                entity.Property(e => e.AssetsId).HasColumnType("int(11)");

                entity.Property(e => e.IsApproved).HasColumnType("tinyint(1)");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.HasOne(d => d.AssetStatus)
                    .WithMany(p => p.Approvals)
                    .HasForeignKey(d => d.AssetStatusId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("approvalsfk3");

                entity.HasOne(d => d.Assets)
                    .WithMany(p => p.Approvals)
                    .HasForeignKey(d => d.AssetsId)
                    .HasConstraintName("approvalsfk1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Approvals)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("approvalsfk2");
            });

            modelBuilder.Entity<Approvalstages>(entity =>
            {
                entity.ToTable("approvalstages");

                entity.HasIndex(e => e.AssetStatusId)
                    .HasName("approvalstagesfk2");

                entity.HasIndex(e => e.RoleId)
                    .HasName("approvalstagesfk1");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AssetStatusId).HasColumnType("int(11)");

                entity.Property(e => e.RoleId).HasColumnType("int(11)");

                entity.HasOne(d => d.AssetStatus)
                    .WithMany(p => p.Approvalstages)
                    .HasForeignKey(d => d.AssetStatusId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("approvalstagesfk2");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Approvalstages)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("approvalstagesfk1");
            });

            modelBuilder.Entity<Aspnetroles>(entity =>
            {
                entity.ToTable("aspnetroles");

                entity.Property(e => e.Id).HasColumnType("varchar(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(256)");
            });

            modelBuilder.Entity<Aspnetuserclaims>(entity =>
            {
                entity.ToTable("aspnetuserclaims");

                entity.HasIndex(e => e.Id)
                    .HasName("Id")
                    .IsUnique();

                entity.HasIndex(e => e.UserId)
                    .HasName("UserId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ClaimType).HasColumnType("longtext");

                entity.Property(e => e.ClaimValue).HasColumnType("longtext");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnType("varchar(128)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Aspnetuserclaims)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("ApplicationUser_Claims");
            });

            modelBuilder.Entity<Aspnetuserlogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey, e.UserId })
                    .HasName("PRIMARY");

                entity.ToTable("aspnetuserlogins");

                entity.HasIndex(e => e.UserId)
                    .HasName("ApplicationUser_Logins");

                entity.Property(e => e.LoginProvider).HasColumnType("varchar(128)");

                entity.Property(e => e.ProviderKey).HasColumnType("varchar(128)");

                entity.Property(e => e.UserId).HasColumnType("varchar(128)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Aspnetuserlogins)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("ApplicationUser_Logins");
            });

            modelBuilder.Entity<Aspnetuserroles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId })
                    .HasName("PRIMARY");

                entity.ToTable("aspnetuserroles");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IdentityRole_Users");

                entity.Property(e => e.UserId).HasColumnType("varchar(128)");

                entity.Property(e => e.RoleId).HasColumnType("varchar(128)");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Aspnetuserroles)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("IdentityRole_Users");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Aspnetuserroles)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("ApplicationUser_Roles");
            });

            modelBuilder.Entity<Aspnetusers>(entity =>
            {
                entity.ToTable("aspnetusers");

                entity.Property(e => e.Id).HasColumnType("varchar(128)");

                entity.Property(e => e.AccessFailedCount).HasColumnType("int(11)");

                entity.Property(e => e.Email).HasColumnType("varchar(256)");

                entity.Property(e => e.EmailConfirmed).HasColumnType("tinyint(1)");

                entity.Property(e => e.LockoutEnabled).HasColumnType("tinyint(1)");

                entity.Property(e => e.LockoutEndDateUtc).HasColumnType("datetime");

                entity.Property(e => e.PasswordHash).HasColumnType("longtext");

                entity.Property(e => e.PhoneNumber).HasColumnType("longtext");

                entity.Property(e => e.PhoneNumberConfirmed).HasColumnType("tinyint(1)");

                entity.Property(e => e.SecurityStamp).HasColumnType("longtext");

                entity.Property(e => e.TwoFactorEnabled).HasColumnType("tinyint(1)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("varchar(256)");
            });

            modelBuilder.Entity<Assetpayments>(entity =>
            {
                entity.ToTable("assetpayments");

                entity.HasIndex(e => e.AssetId)
                    .HasName("transactionsfk2");

                entity.HasIndex(e => e.FeesId)
                    .HasName("transactionsfk3");

                entity.HasIndex(e => e.UserId)
                    .HasName("transactionsfk1");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Amount).HasColumnType("decimal(10,3)");

                entity.Property(e => e.AssetId).HasColumnType("int(11)");

                entity.Property(e => e.FeesId).HasColumnType("int(11)");

                entity.Property(e => e.IsPayment)
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PaymentDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.PaymentMethod).HasColumnType("int(11)");

                entity.Property(e => e.TransactionId).HasColumnType("int(11)");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.Assetpayments)
                    .HasForeignKey(d => d.AssetId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetpaymentsfk3");

                entity.HasOne(d => d.Fees)
                    .WithMany(p => p.Assetpayments)
                    .HasForeignKey(d => d.FeesId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetpaymentsfk2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Assetpayments)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetpaymentsfk1");
            });

            modelBuilder.Entity<Assets>(entity =>
            {
                entity.ToTable("assets");

                entity.HasIndex(e => e.AssetTypeId)
                    .HasName("assetsfk6");

                entity.HasIndex(e => e.CompanyId)
                    .HasName("assetsfk1");

                entity.HasIndex(e => e.ContactUserId)
                    .HasName("assetsfk2");

                entity.HasIndex(e => e.LeaseId)
                    .HasName("assetsfk4");

                entity.HasIndex(e => e.PaymentStatusId)
                    .HasName("assetsfk7");

                entity.HasIndex(e => e.RoadId)
                    .HasName("assetsfk3");

                entity.HasIndex(e => e.StatusId)
                    .HasName("assetsfk5");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ApplicationDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.AssetTypeId).HasColumnType("int(11)");

                entity.Property(e => e.Balance)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.CompanyId).HasColumnType("int(11)");

                entity.Property(e => e.ContactUserId).HasColumnType("int(11)");

                entity.Property(e => e.Height).HasColumnType("decimal(10,2)");

                entity.Property(e => e.Latitude).HasColumnType("varchar(20)");

                entity.Property(e => e.LeaseId).HasColumnType("int(11)");

                entity.Property(e => e.Length)
                    .HasColumnName("length")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.LocationDescription).HasColumnType("varchar(1000)");

                entity.Property(e => e.Longitude).HasColumnType("varchar(20)");

                entity.Property(e => e.PaymentStatusId)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'2'");

                entity.Property(e => e.RoadId).HasColumnType("int(11)");

                entity.Property(e => e.SchedulerLastModifiedMonthNumber)
                    .HasColumnName("scheduler_last_modified_month_number")
                    .HasColumnType("int(2)");

                entity.Property(e => e.StatusId).HasColumnType("int(11)");

                entity.Property(e => e.Title).HasColumnType("varchar(50)");

                entity.Property(e => e.Type).HasColumnType("int(11)");

                entity.Property(e => e.Used)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.Width).HasColumnType("decimal(10,2)");

                entity.HasOne(d => d.AssetType)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.AssetTypeId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetsfk6");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetsfk1");

                entity.HasOne(d => d.ContactUser)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.ContactUserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetsfk2");

                entity.HasOne(d => d.Lease)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.LeaseId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetsfk4");

                entity.HasOne(d => d.PaymentStatus)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.PaymentStatusId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetsfk7");

                entity.HasOne(d => d.Road)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.RoadId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetsfk3");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("assetsfk5");
            });

            modelBuilder.Entity<Assetstatuses>(entity =>
            {
                entity.ToTable("assetstatuses");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Title).HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<Assettypes>(entity =>
            {
                entity.ToTable("assettypes");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Title).HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<Companies>(entity =>
            {
                entity.ToTable("companies");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Balance)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.Cr14)
                    .HasColumnName("CR14")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.Cr6)
                    .HasColumnName("CR6")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.Email).HasColumnType("varchar(30)");

                entity.Property(e => e.Name).HasColumnType("varchar(100)");

                entity.Property(e => e.Phone).HasColumnType("varchar(20)");

                entity.Property(e => e.Status).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Fees>(entity =>
            {
                entity.ToTable("fees");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Amount).HasColumnType("decimal(10,2)");

                entity.Property(e => e.Description).HasColumnType("varchar(1000)");

                entity.Property(e => e.Title).HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<Lease>(entity =>
            {
                entity.ToTable("lease");

                entity.HasIndex(e => e.AssetId)
                    .HasName("leasefk2");

                entity.HasIndex(e => e.LeaseTypeId)
                    .HasName("leasefk1");

                entity.HasIndex(e => e.StatusId)
                    .HasName("leasefk3");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.AssetId).HasColumnType("int(11)");

                entity.Property(e => e.CommencementDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.IsPenaltyFees)
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.LeaseTypeId).HasColumnType("int(11)");

                entity.Property(e => e.Name).HasColumnType("varchar(100)");

                entity.Property(e => e.StatusId).HasColumnType("int(11)");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.LeaseNavigation)
                    .HasForeignKey(d => d.AssetId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("leasefk2");

                entity.HasOne(d => d.LeaseType)
                    .WithMany(p => p.Lease)
                    .HasForeignKey(d => d.LeaseTypeId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("leasefk1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Lease)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("leasefk3");
            });

            modelBuilder.Entity<Leasestatuses>(entity =>
            {
                entity.ToTable("leasestatuses");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Title).HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<Leasetypes>(entity =>
            {
                entity.ToTable("leasetypes");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Amount).HasColumnType("decimal(10,2)");

                entity.Property(e => e.Description).HasColumnType("varchar(1000)");

                entity.Property(e => e.Distance).HasColumnType("decimal(10,2)");

                entity.Property(e => e.Length).HasColumnType("decimal(10,2)");

                entity.Property(e => e.PenaltyFees).HasColumnType("decimal(10,2)");

                entity.Property(e => e.Title).HasColumnType("varchar(100)");

                entity.Property(e => e.Width).HasColumnType("decimal(10,2)");

                entity.Property(e => e.Years).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Notifications>(entity =>
            {
                entity.ToTable("notifications");

                entity.HasIndex(e => e.UserId)
                    .HasName("notificationsfk1");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CompanyId).HasColumnType("int(11)");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.IsRead)
                    .HasColumnName("isRead")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Message).HasColumnType("longtext");

                entity.Property(e => e.NotificationType).HasColumnType("int(11)");

                entity.Property(e => e.Title).HasColumnType("varchar(200)");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("notificationsfk1");
            });

            modelBuilder.Entity<Paymentstatuses>(entity =>
            {
                entity.ToTable("paymentstatuses");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Title).HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<Roads>(entity =>
            {
                entity.ToTable("roads");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Title).HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.ToTable("roles");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Title).HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<Transactions>(entity =>
            {
                entity.ToTable("transactions");

                entity.HasIndex(e => e.ApprovedByUserId)
                    .HasName("transactionsfk4");

                entity.HasIndex(e => e.FeesId)
                    .HasName("transactionsfk3");

                entity.HasIndex(e => e.UserId)
                    .HasName("transactionsfk1");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Amount).HasColumnType("decimal(10,3)");

                entity.Property(e => e.ApprovedByUserId).HasColumnType("int(11)");

                entity.Property(e => e.ApprovedDate).HasColumnType("datetime");

                entity.Property(e => e.AssetId).HasColumnType("int(11)");

                entity.Property(e => e.ClientEmail).HasColumnType("varchar(50)");

                entity.Property(e => e.FeesId).HasColumnType("int(11)");

                entity.Property(e => e.Filename).HasColumnType("varchar(1000)");

                entity.Property(e => e.IsApproved)
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.IsPayment)
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.IsRejected)
                    .HasColumnName("isRejected")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.PaymentDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.PaymentMethod).HasColumnType("int(11)");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.HasOne(d => d.ApprovedByUser)
                    .WithMany(p => p.TransactionsApprovedByUser)
                    .HasForeignKey(d => d.ApprovedByUserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("transactionsfk4");

                entity.HasOne(d => d.Fees)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.FeesId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("transactionsfk3");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TransactionsUser)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("transactionsfk1");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.CompanyId)
                    .HasName("usersfk1");

                entity.HasIndex(e => e.IdentityUserId)
                    .HasName("usersfk3");

                entity.HasIndex(e => e.RoleId)
                    .HasName("usersfk2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CompanyId).HasColumnType("int(11)");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.Firstname).HasColumnType("varchar(254)");

                entity.Property(e => e.IdentityUserId).HasColumnType("varchar(128)");

                entity.Property(e => e.Phone).HasColumnType("varchar(20)");

                entity.Property(e => e.RoleId).HasColumnType("int(11)");

                entity.Property(e => e.Surname).HasColumnType("varchar(254)");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("usersfk1");

                entity.HasOne(d => d.IdentityUser)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.IdentityUserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("usersfk3");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("usersfk2");
            });
        }
    }
}
