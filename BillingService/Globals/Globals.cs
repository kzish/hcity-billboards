﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillingService.Models
{
    public class Globals
    {
        public static string log_path = @"c:\rubiem\logs\hcity_logs.txt";
        public static string notification_service_base_url = "https://localhost:4002";


        public static void log_data_to_file(string source,string data)
        {
            System.IO.File.AppendAllText(log_path,$"source:{source}   Data:{data}"+Environment.NewLine);
        }


    }


    

}
