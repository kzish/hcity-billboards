﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BillingService.Models;
using Microsoft.AspNetCore.Mvc;

namespace BillingService.Controllers
{
    [Route("Home")]
    [Route("")]
    public class HomeController : Controller
    {
        [Route("")]
        public IActionResult Index()
        {
            try
            {
                return Ok("Billing service is running");
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }



    }
}
