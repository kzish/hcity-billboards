﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Http;
using BillingService.Models;
using System.Net.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace BillingService.Services
{
    public class sTimer : IHostedService, IDisposable
    {

        Timer timer = null;
        //static private dbContext db = new dbContext();
        static bool busy;

        //constructor
        public sTimer()
        {

        }


        //this task will deduct the balance on schedule
        public void RunTask(object state)
        {
            if (busy)
            {
                return;//if im busy wait by not running this code
            }

            try
            {
                busy = true;

                //get the current day of the month
                var day_of_month = DateTime.Now.Day;
                //only deduct the amount if the first day of the month
                if (day_of_month == 1)
                {

                    var db = new dbContext();
                    //get the active assets which have not already been modified by the scheduler for this month
                    var active_assets = db.Assets.Include(l => l.Lease).Where(i => i.StatusId == 3
                                          && i.SchedulerLastModifiedMonthNumber != DateTime.Now.Month
                                          && i.LeaseId != null)//the asset must have a lease
                                        .ToList();//3 = active status, and month not todays month



                    //deduct the amount from the assests balance and add the same amount to the used for each asset
                    foreach (var asset in active_assets)
                    {
                        asset.Balance = (asset.Balance - asset.Lease.Amount);
                        asset.Used = (asset.Used + asset.Lease.Amount);

                        //set the scheduler_last_modified_month_number flag
                        asset.SchedulerLastModifiedMonthNumber = DateTime.Now.Month;
                        //reset the used to zero if the balance goes into the negative
                        if (asset.Balance < 0) asset.Used = 0;
                        db.SaveChanges();//persist to db
                        //check the balances sync not async
                        CheckBalanceLevel(asset);
                    }

                    
                    db.Dispose();



                }


            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("BillingService.Services.RunTask", ex.Message);
            }
            finally
            {
                busy = false;
            }


        }//runtask

        //this method sends email notification to the owners of the 
        //asset to notify them of the balance level of the assets
        public void CheckBalanceLevel(Assets asset)
        {
            //if the balance falls to zero, or one month, or two months send the client a notification
            //send also the admin a notification
            var db = new dbContext();
            var owner_email_address = db.Companies.Where(i => i.Id == asset.CompanyId).FirstOrDefault().Email;

            try
            {
                //at zero or below
                //send soft email to asset owner
                if (asset.Balance <= 0)
                {
                    var client = new HttpClient();
                    var email = new mEmailMessage();
                    email.to_email = owner_email_address;
                    email.message = $@"Dear valued customer. Your balance for the asset {asset.Title}
                                has depleted. Kindly upload a proof of payment to topup your account
                                before the end of {DateTime.Now.ToString("MMMM")} to avoid legal action bieng taken.
                                Thank you for your coorporation.";
                    var res = client.PostAsJsonAsync<mEmailMessage>($"{Globals.notification_service_base_url}/Notification/SendEmail", email).Result.Content.ReadAsStringAsync().Result;
                }

                //at 30 days or more
                //1=30days overdue
                //2=60days overdue
                //3=90days overdue
                //4=120days overdue

               
                var _30_days_over_due = (-1 * asset.Lease.Amount);
                var _60_days_over_due = (-2 * asset.Lease.Amount);
                var _90_days_over_due = (-3 * asset.Lease.Amount);
                var _120_days_over_due = (-4 * asset.Lease.Amount);

                var over_due_by_days = 0;

                if (asset.Balance <= _30_days_over_due) over_due_by_days = 30;
                if (asset.Balance <= _60_days_over_due) over_due_by_days = 60;
                if (asset.Balance <= _90_days_over_due) over_due_by_days = 90;
                if (asset.Balance <= _120_days_over_due) over_due_by_days = 120;


                //change the status of the asset
                asset.PaymentStatusId = 1;//payed up
                if (asset.Balance <= _30_days_over_due) asset.PaymentStatusId = 2;//30 days
                if (asset.Balance <= _60_days_over_due) asset.PaymentStatusId = 3;//60 days
                if (asset.Balance <= _90_days_over_due) asset.PaymentStatusId = 4;//90 days
                if (asset.Balance == _120_days_over_due) asset.PaymentStatusId = 5;//120 days
                if (asset.Balance < _120_days_over_due) asset.PaymentStatusId = 8;//Legal Action
                db.Entry(asset).State = EntityState.Modified;
                db.SaveChanges();
                db.Dispose();

                //todo: add link to click and auto sign in
                //todo: also send email to the admin console user
                //send hard email to asset owner
                if (asset.Balance <= _30_days_over_due)
                {
                    var client = new HttpClient();
                    var email = new mEmailMessage();
                    email.to_email = owner_email_address;
                    email.message = $@"Dear valued customer. Your balance for the asset {asset.Title}
                                is overdue by {over_due_by_days} days. You are required to pay
                                the outstanding fees, or face legal action.";

                    var res = client.PostAsJsonAsync<mEmailMessage>($"{Globals.notification_service_base_url}/Notification/SendEmail", email).Result.Content.ReadAsStringAsync().Result;
                    //Globals.log_data_to_file("line 182", res);
                }
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("BillingService.Services.CheckBalanceLevel", ex.Message);
            }

        }//checkBalance.

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var autoEvent = new AutoResetEvent(false);
            timer = new Timer(RunTask, autoEvent, TimeSpan.Zero, TimeSpan.FromSeconds(10));//1 sec timer
            Globals.log_data_to_file("BillingService.Services.StartAsync", "Biller service started");
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            timer?.Dispose();
        }
    }//sTimer

}//namespace
