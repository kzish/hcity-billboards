﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace NotificationService.Models
{
    public class Globals
    {
        public static string log_path = @"c:\rubiem\logs\hcity_logs.txt";
        public static string notification_service_base_url = "https://localhost:4002";


        public static void log_data_to_file(string source, string data)
        {
            System.IO.File.AppendAllText(log_path, $"source:{source}   Data:{data}" + Environment.NewLine);
        }

        public static bool SendEmail(mEmailMessage email)
        {
            //clean up
            email.message = email.message.Replace("<br>", "/n");
            email.message = email.message.Replace("<br/>", "/n");

            try
            {
                new SmtpClient
                {
                    Host = "Smtp.Gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(email.from_email, email.password)
                }.Send(new MailMessage { IsBodyHtml = true, 
                                        From = new MailAddress(email.from_email, email.from_name), 
                                        To = { email.to_email}, 
                                        Subject = email.subject, 
                                        Body = email.message, 
                                        BodyEncoding = Encoding.UTF8 
                                        });

            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }



    }
}