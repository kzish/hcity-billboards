﻿using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using NotificationService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using NotificationServiceApi.Models;

namespace NotificationService.Controllers
{
    [Route("Notification")]
    public class NotificationController : Controller
    {

        private HostingEnvironment host;
        private dbContext db = new dbContext();

        public NotificationController(HostingEnvironment e)
        {
            host = e;
        }

        /// <summary>
        /// send an email to the recipient
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("SendEmail")]
        public JsonResult SendEmail([FromBody]mEmailMessage email)
        {
            try
            {


                //also send the notification as you send the email
                var notification = new Notifications();
                notification.Title = email.subject;
                notification.Message = email.message;
                notification.Date = DateTime.Now;
                notification.CompanyId =  db.Companies.Where(i => i.Email == email.to_email).FirstOrDefault()?.Id;
                notification.IsRead = false;
                SendNotification(notification);

                //send email
                string email_path = email.get_email_path();
                string email_text = System.IO.File.ReadAllText(host.WebRootPath + email_path);
                email.message = email_text.Replace("{{message}}", email.message);
                Globals.SendEmail(email);

               
                
                return Json(new
                {
                    res = "ok",
                    msg = "email sent"
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("NotificationService.Controllers.SendEmail", ex.Message);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// send a notification to the user
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        [HttpPost("SendNotification")]
        public JsonResult SendNotification([FromBody]Notifications notification)
        {
            try
            {
                notification.IsRead = false;
                notification.Date = DateTime.Now;
                db.Notifications.Add(notification);
                db.SaveChanges();
                return Json(new { res = "ok", msg = "Notification sent" });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file("NotificationService.Controllers.SendNotification", ex.Message);
                return Json(new { res = "err", msg = ex.Message });
            }
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


    }
}