﻿using System;
using System.Collections.Generic;

namespace NotificationServiceApi.Models
{
    public partial class Roads
    {
        public Roads()
        {
            Assets = new HashSet<Assets>();
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Assets> Assets { get; set; }
    }
}
