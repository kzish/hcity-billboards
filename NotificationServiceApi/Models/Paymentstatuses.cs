﻿using System;
using System.Collections.Generic;

namespace NotificationServiceApi.Models
{
    public partial class Paymentstatuses
    {
        public Paymentstatuses()
        {
            Assets = new HashSet<Assets>();
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Assets> Assets { get; set; }
    }
}
