﻿using System;
using System.Collections.Generic;

namespace NotificationServiceApi.Models
{
    public partial class Leasetypes
    {
        public Leasetypes()
        {
            Lease = new HashSet<Lease>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }
        public decimal? PenaltyFees { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public decimal? Distance { get; set; }
        public int? Years { get; set; }

        public virtual ICollection<Lease> Lease { get; set; }
    }
}
