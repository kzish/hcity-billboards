﻿using System;
using System.Collections.Generic;

namespace NotificationServiceApi.Models
{
    public partial class Notifications
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime? Date { get; set; }
        public int? UserId { get; set; }
        public int? NotificationType { get; set; }
        public int? CompanyId { get; set; }
        public bool IsRead { get; set; }

        public virtual Users User { get; set; }
    }
}
