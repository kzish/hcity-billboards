﻿using System;
using System.Collections.Generic;

namespace NotificationServiceApi.Models
{
    public partial class Roles
    {
        public Roles()
        {
            Approvalstages = new HashSet<Approvalstages>();
            Users = new HashSet<Users>();
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Approvalstages> Approvalstages { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
