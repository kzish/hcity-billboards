﻿using System;
using System.Collections.Generic;

namespace NotificationServiceApi.Models
{
    public partial class Companies
    {
        public Companies()
        {
            Assets = new HashSet<Assets>();
            Users = new HashSet<Users>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? Status { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Cr6 { get; set; }
        public string Cr14 { get; set; }
        public decimal? Balance { get; set; }

        public virtual ICollection<Assets> Assets { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
