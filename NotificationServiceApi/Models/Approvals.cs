﻿using System;
using System.Collections.Generic;

namespace NotificationServiceApi.Models
{
    public partial class Approvals
    {
        public int Id { get; set; }
        public int AssetsId { get; set; }
        public int UserId { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public sbyte IsApproved { get; set; }
        public int? AssetStatusId { get; set; }

        public virtual Assetstatuses AssetStatus { get; set; }
        public virtual Assets Assets { get; set; }
        public virtual Users User { get; set; }
    }
}
