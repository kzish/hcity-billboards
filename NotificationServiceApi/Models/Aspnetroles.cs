﻿using System;
using System.Collections.Generic;

namespace NotificationServiceApi.Models
{
    public partial class Aspnetroles
    {
        public Aspnetroles()
        {
            Aspnetuserroles = new HashSet<Aspnetuserroles>();
        }

        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Aspnetuserroles> Aspnetuserroles { get; set; }
    }
}
