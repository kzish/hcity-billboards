﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientMVC.Models
{
    public class Globals
    {

        public static string log_path = @"c:\rubiem\logs\hcity_logs.txt";
        public static string notification_service_base_url = "https://localhost:4002";


        public static void log_data_to_file(string source, string data)
        {
            System.IO.File.AppendAllText(log_path, $"source:{source}   Data:{data}" + Environment.NewLine);
        }

        public static lease get_lease_from_id(int id)
        {
            var db = new hcityEntities();
            var lease = db.leases.Find(id);
            db.Dispose();
            return lease ?? new lease() { Name = "" };
        }
        public static string get_road_name(int? id)
        {
            var db = new hcityEntities();
            var item = db.roads.Find(id);
            db.Dispose();
            return item?.Title;
        } 
        
        public static string get_payment_status_name(int? id)
        {
            var db = new hcityEntities();
            var item = db.paymentstatuses.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_asset_status_name(int? id)
        {
            var db = new hcityEntities();
            var item = db.assetstatuses.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_fees_name(int? id)
        {
            var db = new hcityEntities();
            var item = db.fees.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_asset_name(int? id)
        {
            var db = new hcityEntities();
            var item = db.assets.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_asset_type_name(int? id)
        {
            var db = new hcityEntities();
            var item = db.assettypes.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_company_name(int? id)
        {
            var db = new hcityEntities();
            var item = db.companies.Find(id);
            db.Dispose();
            return item?.Name;
        }


    }
}