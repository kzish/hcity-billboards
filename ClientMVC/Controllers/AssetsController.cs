﻿using ClientMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace ClientMVC.Controllers
{
    [Authorize]
    [RoutePrefix("Assets")]
    public class AssetsController : Controller
    {
        private hcityEntities db = new hcityEntities();

        [Route("CreateAsset")]
        [HttpGet]
        public ActionResult CreateAsset()
        {
            ViewBag.title = "Create Asset";

            //get asset statuses
            ViewBag.asset_statutes = db.assetstatuses.ToList();
            //get asset types
            ViewBag.asset_types = db.assettypes.ToList();
            //get roads
            ViewBag.roads = db.roads.ToList();
            var company = db.companies.Where(i => i.Email == User.Identity.Name).First();
            ViewBag.CompanyId = company.Id;


            return View();
        }


        [Route("CreateAsset")]
        [HttpPost]
        public ActionResult CreateAsset(asset Asset)
        {
            try
            {
                Asset.ApplicationDate = DateTime.Now;
                Asset.StatusId = 7;//application: status of the asset it begins in application state then advances from there
                Asset.PaymentStatusId = 10;//application: 
                Asset.Balance = 0;
                Asset.Used = 0;
                db.assets.Add(Asset);
                db.SaveChanges();
                ViewBag.title = "Create Asset";
                TempData["tmsg"] = "Asset created";
                TempData["type"] = "success";

                //send email notification to admin to alert new payment is made
                var admin_emails = db.admin_emails.ToList();
                foreach (var email_ in admin_emails)
                {
                    Task.Run(() => {
                        var client = new HttpClient();
                        var email = new mEmailMessage();
                        email.to_email = email_.email;
                        email.message = $@"A new Asset application has been made by {Globals.get_company_name( Asset.CompanyId)}, Title:{Asset.Title}.
                                 on {Asset.ApplicationDate?.ToString("yyyy-MM-dd")}. Log in to view details.
                                ";
                        var res = client.PostAsJsonAsync<mEmailMessage>($"{Globals.notification_service_base_url}/Notification/SendEmail", email).Result.Content.ReadAsStringAsync().Result;
                    });
                }
            }
            catch (Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }
            return RedirectToAction("CreateAsset");

        }


        [Route("ViewLease/{asset_id}")]
        public ActionResult ViewLease(int asset_id)
        {
            ViewBag.Title = "View Lease";
            //ensure you can only see what is yours
            var company = db.companies.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            var asset = db.assets.Where(i => i.Id == asset_id && i.CompanyId == company.Id).FirstOrDefault()??new Models.asset();
            var lease = db.leases.Where(i => i.AssetId == asset.Id).FirstOrDefault()??new Models.lease();
            ViewBag.lease = lease;
            return View();
        }

        [Route("ViewAsset/{id}")]
        public ActionResult ViewAsset(int id)
        {
            ///ensure user can only view his/her own assets
            var company = db.companies.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            ViewBag.title = "View Asset";
            var asset = db.assets.Where(i => i.Id == id && i.CompanyId == company.Id).FirstOrDefault();
            ViewBag.asset = asset??new asset();
            return View();
        }

        [Route("ListAllMyAssets")]
        [HttpGet]
        public ActionResult ListAllMyAssets(string date_from,string date_to,int status_id=-1)
        {
            ViewBag.title = "My Assets";
            var company = db.companies.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            var assets = db.assets.Where(i => i.CompanyId == company.Id);

            if(!string.IsNullOrEmpty(date_from))
            {
                var date_from_ = DateTime.Parse(date_from);
                assets = assets.Where(i => i.ApplicationDate >= date_from_);
            }

            if (!string.IsNullOrEmpty(date_to))
            {
                var date_to_ = DateTime.Parse(date_to);
                assets = assets.Where(i => i.ApplicationDate <= date_to_);
            }

            if(status_id>-1)
            {
                assets = assets.Where(i => i.StatusId == status_id);
            }
            ViewBag.asset_statuses = db.assetstatuses.ToList();
            ViewBag.assets = assets.ToList();
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            ViewBag.selected_status = status_id;
            ViewBag.db = db;
            return View();
        }


        [HttpGet][Route("EditAsset/{id}")]
        public ActionResult EditAsset(int id)
        {
            //get asset statuses
            ViewBag.asset_statutes = db.assetstatuses.ToList();
            //get asset types
            ViewBag.asset_types = db.assettypes.ToList();
            //get roads
            ViewBag.roads = db.roads.ToList();
            var company = db.companies.Where(i => i.Email == User.Identity.Name).First();
            ViewBag.CompanyId = company.Id;

            ViewBag.title = "Edit Asset";
            var asset = db.assets.Where(i => i.Id == id).FirstOrDefault();
            ViewBag.asset = asset;
            return View();
        }


        [Route("DeleteAsset/{id}")]
        public ActionResult DeleteAsset(int id)
        {
            try
            {
                var asset = db.assets.Where(i => i.Id == id).FirstOrDefault();
                db.assets.Remove(asset);
                db.SaveChanges();
                TempData["tmsg"] = "Asset Removed";
                TempData["type"] = "success";
            }
            catch(Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }

            return RedirectToAction("ListAllMyAssets");

        }


        [HttpPost][Route("EditAsset")]
        public ActionResult EditAsset(asset Asset)
        {
            try
            {
                //update the changed fields of the asset
                var _Asset = db.assets.Where(i => i.Id == Asset.Id).First();

                _Asset.Title = Asset.Title;
                _Asset.LocationDescription = Asset.LocationDescription;
                _Asset.Longitude = Asset.Longitude;
                _Asset.Latitude = Asset.Latitude;
                _Asset.length = Asset.length;
                _Asset.Width = Asset.Width;
                _Asset.AssetTypeId = Asset.AssetTypeId;
                _Asset.RoadId = Asset.RoadId;
                
                db.SaveChanges();
                TempData["tmsg"] = "Asset saved";
                TempData["type"] = "success";
            }
            catch (Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }
            return RedirectToAction("ListAllMyAssets");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }





    }
}
