﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Newtonsoft.Json;
using ClientMVC.Models;
using System.Globalization;
using System.Runtime.Remoting.Contexts;

namespace ClientMVC.Controllers
{
    [RoutePrefix("Notifications")]
    [Authorize]
    public class NotificationsController : Controller
    {

        private hcityEntities db = new hcityEntities();

        [Route("AllNotifications")]
        [HttpGet]
        public ActionResult AllNotifications(string date_from,string date_to)
        {
            ViewBag.title = "Notifications / All Notifications";

            if (date_from == "-1") date_from = null;
            if (date_to == "-1") date_to = null;

            var company = db.companies.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            var notifications = db.notifications.Where(i => i.CompanyId == (int)company.Id );
            if (!string.IsNullOrEmpty(date_from))
            {
                var _date_from = DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                notifications = notifications.Where(i => i.Date
                    >= _date_from
                    );
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                var _date_to = DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                notifications = notifications.Where(i => i.Date
                    <= _date_to
                    );
            }
            ViewBag.notifications = notifications.ToList();
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            return View();
        }



        /// <summary>
        /// show the notifications that have been read
        /// </summary>
        /// <returns></returns>
        [Route("Read")]
        [HttpGet]
        public ActionResult Read(string date_from, string date_to)
        {
            ViewBag.title = "Notifications / Read";


            if (date_from == "-1") date_from = null;
            if (date_to == "-1") date_to = null;

            var company = db.companies.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            var notifications = db.notifications.Where(i => i.CompanyId == (int)company.Id && i.isRead == true);

            if (!string.IsNullOrEmpty(date_from))
            {
                var _date_from = DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                notifications = notifications.Where(i => i.Date
                    >= _date_from
                    );
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                var _date_to = DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                notifications = notifications.Where(i => i.Date
                    <= _date_to
                    );
            }
            ViewBag.notifications = notifications.ToList();
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            return View();
        }


        /// <summary>
        /// show the notifications that have not been read
        /// </summary>
        /// <returns></returns>
        [Route("UnRead")]
        [HttpGet]
        public ActionResult UnRead(string date_from, string date_to)
        {
            ViewBag.title = "Notifications / UnRead";

            if (date_from == "-1") date_from = null;
            if (date_to == "-1") date_to = null;

            var company = db.companies.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            var notifications = db.notifications.Where(i => i.CompanyId == (int)company.Id && i.isRead == false);
            if (!string.IsNullOrEmpty(date_from))
            {
                var _date_from = DateTime.ParseExact(date_from, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                notifications = notifications.Where(i => i.Date
                    >= _date_from
                    );
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                var _date_to = DateTime.ParseExact(date_to, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                notifications = notifications.Where(i => i.Date
                    <= _date_to
                    );
            }
            ViewBag.notifications = notifications.ToList();
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            return View();
        }


        /// <summary>
        /// view a single notification
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("ViewDetails/{id}")]
        public ActionResult ViewDetails(int id)
        {
            ViewBag.title = "Notification";
            //also mark the notifiation as read
            var notification = db.notifications.Where(i => i.Id == id).FirstOrDefault();
            notification.isRead = true;
            db.SaveChanges();
            ViewBag.notification = notification;
            return View();
        }

        [Route("MarkAsRead/{id}/{date_from?}/{date_to?}/")]
        public ActionResult MarkAsRead(int id, string date_from, string date_to)
        {
            try
            {
                if (date_from == "-1") date_from = null;
                if (date_to == "-1") date_to = null;

                var notification = db.notifications.Where(i => i.Id == id).FirstOrDefault();
                notification.isRead = true;
                db.SaveChanges();
                ViewBag.date_from = date_from;
                ViewBag.date_to = date_to;
                TempData["tmsg"] = "Saved";
                TempData["type"] = "success";
            }
            catch (Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            return RedirectToAction("AllNotifications", "Notifications", new { date_from, date_to });

        }

        [Route("MarkAsUnRead/{id}/{date_from?}/{date_to?}/")]
        public ActionResult MarkAsUnRead(int id, string date_from, string date_to)
        {
            try
            {
                if (date_from == "-1") date_from = null;
                if (date_to == "-1") date_to = null;

                var notification = db.notifications.Where(i => i.Id == id).FirstOrDefault();
                notification.isRead = false;
                db.SaveChanges();
                ViewBag.date_from = date_from;
                ViewBag.date_to = date_to;
                TempData["tmsg"] = "Saved";
                TempData["type"] = "success";
            }
            catch (Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            return RedirectToAction("AllNotifications", "Notifications", new { date_from, date_to });

        }



        [Route("Delete/{id}/{date_from?}/{date_to?}/")]
        public ActionResult Delete(int id, string date_from, string date_to)
        {
            try
            {
                if (date_from == "-1") date_from = null;
                if (date_to == "-1") date_to = null;

                var notification = db.notifications.Where(i => i.Id == id).FirstOrDefault();
                db.notifications.Remove(notification);
                db.SaveChanges();
                ViewBag.date_from = date_from;
                ViewBag.date_to = date_to;
                TempData["tmsg"] = "Deleted";
                TempData["type"] = "success";
            }
            catch (Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            
            return RedirectToAction("AllNotifications", "Notifications", new { date_from, date_to });
        }



        [Route("count_number_of_unread_notifications")]
        public int count_number_of_unread_notifications(string email)
        {
            
            var company = db.companies.Where(i => i.Email == email).FirstOrDefault();
            var n = db.notifications.Where(i => i.CompanyId == company.Id && i.isRead==false).ToList().Count;
            return n;
        }

      


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }



    }
}
