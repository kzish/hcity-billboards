﻿using ClientMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace ClientMVC.Controllers
{
    [RoutePrefix("Home")]
    [Authorize]
    public class HomeController : Controller
    {

        private hcityEntities db = new hcityEntities();


        [Route("Dashboard")]
        [Route("")]
        [Route("~/")]//default entry point for the entire app
        public ActionResult Dashboard()
        {
            ViewBag.title = "Dashboard";
            var company = db.companies.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            var assets = db.assets.Where(i => i.CompanyId == company.Id).ToList();



            //notifications
            ViewBag.notifications_read = db.notifications.Where(i => i.CompanyId == company.Id && i.isRead).Count();
            ViewBag.notifications_un_read = db.notifications.Where(i => i.CompanyId == company.Id && !i.isRead).Count();


            //assets
            ViewBag.assets_in_dev_permit = db.assets.Where(i => i.CompanyId == company.Id && i.StatusId == 1).Count();
            ViewBag.assets_in_trench_permit = db.assets.Where(i => i.CompanyId == company.Id && i.StatusId == 2).Count();
            ViewBag.assets_in_active = db.assets.Where(i => i.CompanyId == company.Id && i.StatusId == 3).Count();
            ViewBag.assets_in_decommisioned = db.assets.Where(i => i.CompanyId == company.Id && i.StatusId == 4).Count();
            ViewBag.assets_in_rip_off = db.assets.Where(i => i.CompanyId == company.Id && i.StatusId == 5).Count();
            ViewBag.assets_in_legal_action = db.assets.Where(i => i.CompanyId == company.Id && i.StatusId == 6).Count();
            ViewBag.assets_in_application = db.assets.Where(i => i.CompanyId == company.Id && i.StatusId == 7).Count();

            //balance and used
            ViewBag.total_balance = db.assets.Where(i => i.CompanyId == company.Id && (i.StatusId == 3 || i.StatusId == 6)).ToList().Sum(i => i.Balance);
            ViewBag.total_used = db.assets.Where(i => i.CompanyId == company.Id && (i.StatusId == 3 || i.StatusId == 6)).ToList().Sum(i => i.Used);


            ViewBag.assets = assets;
            return View();
        }


        [Route("About")]
        public ActionResult About()
        {
            ViewBag.title = "About";
            return View();
        }



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }



    }
}