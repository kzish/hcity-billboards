﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

using System.Globalization;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using ClientMVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ClientMVC.Models; 

namespace ClientMVC.Controllers
{
    [Authorize]
    [RoutePrefix("Account")]
    public class AccountController : Controller
    {
        private hcityEntities db = new hcityEntities();

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }



        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }



        //view basic company details

        [Route("AccountDetails")]
        [HttpGet]
        public ActionResult AccountDetails()
        {
            ViewBag.title = "Account Details";
            var company = db.companies.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            ViewBag.company = company;

            var user = db.aspnetusers.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            ViewBag.user = user;
            return View();
        }


        [Route("ChangePassword")]
        [HttpGet]
        public ActionResult ChangePassword()
        {
            ViewBag.title = "Change Password";
            return View();
        }

        [Route("ChangePassword")]
        [HttpPost]
        public ActionResult ChangePassword(string old_password, string new_password)
        {
            ViewBag.title = "Change Password";
            try
            {
                var user_id = User.Identity.GetUserId();
                var res = UserManager.ChangePassword(user_id, old_password, new_password);
                if (res.Succeeded)
                {

                    TempData["tmsg"] = "Password Changed";
                    TempData["type"] = "success";
                    //AuthenticationManager.SignOut();
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie, DefaultAuthenticationTypes.ExternalCookie);
                    return RedirectToAction("Login", "Auth");
                }
                else
                {
                    TempData["tmsg"] = "Wrong Old Password";
                    TempData["type"] = "error";
                }
            }
            catch (Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["ex"] = ex.Message;
                TempData["type"] = "error";
            }
            return View();
        }


        //obsolete
        [Route("SaveCompanyDetails")]
        [HttpPost]
        public async Task<ActionResult> SaveCompanyDetails(string tel, string email)
        {
            try
            {
                var user = db.aspnetusers.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
                var identity_user = UserManager.FindByEmail(User.Identity.Name);
                identity_user.Email = email;
                identity_user.UserName = email;
                user.PhoneNumber = tel;
                user.Email = email;

                //update email 
                // Persiste the changes
                var result = await UserManager.UpdateAsync(identity_user);
                if(result.Succeeded)
                {
                    await db.SaveChangesAsync();
                    TempData["tmsg"] = "Saved";
                    TempData["type"] = "success";
                }
                else
                {
                    TempData["tmsg"] = "Error Email taken";
                    TempData["type"] = "error";
                }
                
            }catch(Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
            }
            return RedirectToAction("AccountDetails");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            db.Dispose();
            base.Dispose(disposing);
        }


    }
}