﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using ClientMVC.Models;

namespace ClientMVC.Controllers
{

    [RoutePrefix("Billing")]
    [Authorize]
    public class BillingController : Controller
    {
        private hcityEntities db = new hcityEntities();

        [Route("UploadPayment")]
        [HttpGet]
        public ActionResult UploadPayment()
        {
            ViewBag.title = "Upload Payment";

            //get the fees type
            ViewBag.fee = db.fees.ToList();
            //get the company
            var company = db.companies.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            //get my assets
            ViewBag.assets = db.assets.Where(i => i.CompanyId == company.Id).ToList();

            return View();
        }



        //upload the payment and the proof of payment
        [Route("UploadPayment")]
        [HttpPost]
        public ActionResult UploadPayment(HttpPostedFileBase file,transaction trans)
        {
            try
            {
                ViewBag.title = "Upload Payment";

                //rename file with guid and maintain the file extension
                var filename = $"~/Content/Uploads/Payments/{Guid.NewGuid().ToString()}.{Path.GetExtension(file.FileName)}";
                var filepath = Server.MapPath(filename);

                
                trans.ClientEmail = User.Identity.Name;
                trans.Filename = filename;//store the relative filename
                db.transactions.Add(trans);
                file.SaveAs(filepath);//save to file path
                db.SaveChanges();
                TempData["tmsg"] = "Saved";
                TempData["type"] = "success";

                //send email notification to admin to alert new payment is made
                var admin_emails = db.admin_emails.ToList();
                foreach(var email_ in admin_emails)
                {
                    Task.Run(() => {
                        var client = new HttpClient();
                        var email = new mEmailMessage();
                        email.to_email = email_.email;
                        email.message = $@"A new Payment has been made by {trans.ClientEmail}, Amount:{trans.Amount}.
                                 For Asset:{Globals.get_asset_name(trans.AssetId)}, Payment date:{trans.PaymentDate?.ToString("yyyy-MM-dd")}  
                                ";
                        var res = client.PostAsJsonAsync<mEmailMessage>($"{Globals.notification_service_base_url}/Notification/SendEmail", email).Result.Content.ReadAsStringAsync().Result;
                    });
                }
                

            }
            catch (Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;

            }
            return RedirectToAction("UploadPayment");
        }


        [HttpGet][Route("PaymentHistory")]
        public ActionResult PaymentHistory()
        {
            ViewBag.title = "Payment History";
            var transactions = db.transactions
                .Where(i => i.ClientEmail == User.Identity.Name)
                .OrderByDescending(i=>i.PaymentDate)
                .ToList();
            ViewBag.transactions = transactions;
            return View();
        }

        [HttpGet][Route("DeletePaymentById/{id}")]
        public ActionResult DeletePaymentById(int id)
        {
            try
            {
                var trans = db.transactions.Where(i => i.Id == id).FirstOrDefault();
                //delete the uploaded resouce aswell
                System.IO.File.Delete(Server.MapPath(trans.Filename));
                db.transactions.Remove(trans);
                db.SaveChanges();
                TempData["tmsg"] = "Deleted";
                TempData["type"] = "success";

            }
            catch(Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;

            }
            return RedirectToAction("PaymentHistory");
        }


        [Route("ViewPaymentDetails/{id}")]
        [HttpGet]
        public ActionResult ViewPaymentDetails(int id)
        {
            ViewBag.title = "View Payment Details";
            var payment = db.transactions.Where(i => i.Id == id).FirstOrDefault();
            ViewBag.payment = payment;
            return View();
        }



        /// <summary>
        /// use this to show the uploaded resource from upload proof of payment 
        /// </summary>
        /// <param name="id"></param>
        [Route("ResourceViewer/{id}")]
        public ActionResult ResourceViewer(int id)
        {
            ViewBag.title = "View Payment Details";
            var payment = db.transactions.Where(i => i.Id == id).FirstOrDefault();
            ViewBag.payment = payment;
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


    }
}