﻿using NotificationService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;

namespace NotificationService.Controllers
{
    [Route("EmailNotification")]
    public class EmailNotificationController : Controller
    {

        [Route("SendEmail")]
        public JsonResult SendEmail([FromBody]mEmailMessage email)
        {
            try
            {
                string email_path = email.get_email_path();
                string email_text = System.IO.File.ReadAllText(HostingEnvironment.MapPath(email_path));
                email.message = email_text.Replace("{{message}}", email.message);
                Globals.SendEmail(email);
                return Json(new
                {
                    res = "ok",
                    msg = "email sent"
                }); ;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }
    }
}