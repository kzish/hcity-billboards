﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationService.Models
{
    /// <summary>
    /// models the email message
    /// </summary>
    public class mEmailMessage
    {
        public string from_email = "postmansentinel@gmail.com";
        public string to_email { get; set; }
        public string subject { get; set; } = "System notification";
        public string message { get; set; }
        public string password = "QazWsxEdc@123";
        public string from_name = "Harare City Billboards";
        public eEmailType email_type { get; set; } = eEmailType.notification;

        public string get_email_path() {
            string email_text = string.Empty;
            if(email_type==eEmailType.notification)
            {
                email_text = "~/Content/email_views/email_notification.html";
            }
            if (email_type == eEmailType.password_reset)
            {
                email_text = "~/Content/email_views/email_reset_password.html";
            }
            if (email_type == eEmailType.thank_you_for_registering)
            {
                email_text = "~/Content/email_views/email_thank_you_for_registering.html";
            }
            return email_text;
        }
    }


        public enum eEmailType
        {
            notification,
            password_reset,
            thank_you_for_registering
        }



}