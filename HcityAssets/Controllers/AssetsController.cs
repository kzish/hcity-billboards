﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;
using Newtonsoft.Json;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class AssetsController : Controller
    {
        private HCityEntities db = new HCityEntities();

        // GET: Assets
        
        public ActionResult Index()
        {
            var assets = db.assets.Include(a => a.company).Include(a => a.user).Include(a => a.road).Include(a => a.lease).Include(a => a.assettype).Include(a => a.assetstatus).Include(a => a.paymentstatus);
            return View(assets.ToList());
        }

        // GET: Assets/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.title = "View Asset";
            var asset = db.assets.Where(i => i.Id==id).FirstOrDefault();
            ViewBag.asset = asset ?? new asset();
            return View();
        }

        // GET: Assets/Create
        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(db.companies, "Id", "Name");
            ViewBag.ContactUserId = new SelectList(db.users, "Id", "Firstname");
            ViewBag.RoadId = new SelectList(db.roads, "Id", "Title");
            ViewBag.LeaseId = new SelectList(db.leases, "Id", "Name");
            ViewBag.AssetTypeId = new SelectList(db.assettypes, "Id", "Title");
            ViewBag.StatusId = new SelectList(db.assetstatuses, "Id", "Title");
            ViewBag.PaymentStatus = new SelectList(db.paymentstatuses, "Id", "Title");
            return View();
        }

        // POST: Assets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(asset asset)
        {
            if (ModelState.IsValid)
            {

                db.assets.Add(asset);
                db.SaveChanges();

                //send email to client
                Task.Run(() => {
                    var db = new HCityEntities();
                    var company = db.companies.Where(i => i.Id == asset.CompanyId).FirstOrDefault();
                    var client = new HttpClient();
                    var email = new mEmailMessage();
                    email.to_email = company.Email;
                    email.message = $@"Dear valued customer. A new asset {asset.Title}
                                has been created under your company. Log into your account to review";

                    var json = JsonConvert.SerializeObject(email);
                    var scontent = new StringContent(json, Encoding.UTF8, "application/json");
                    var res = client.PostAsync($"{Globals.notification_service_base_url}/Notification/SendEmail", scontent).Result;
                    db.Dispose();
                });


                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(db.companies, "Id", "Name", asset.CompanyId);
            ViewBag.ContactUserId = new SelectList(db.users, "Id", "Firstname", asset.ContactUserId);
            ViewBag.RoadId = new SelectList(db.roads, "Id", "Title", asset.RoadId);
            ViewBag.LeaseId = new SelectList(db.leases, "Id", "Name", asset.LeaseId);
            ViewBag.AssetTypeId = new SelectList(db.assettypes, "Id", "Title", asset.AssetTypeId);
            ViewBag.StatusId = new SelectList(db.assetstatuses, "Id", "Title", asset.StatusId);
            ViewBag.PaymentStatus = new SelectList(db.paymentstatuses, "Id", "Title", asset.PaymentStatusId);
            return View(asset);
        }

        // GET: Assets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            asset asset = db.assets.Find(id);
            if (asset == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.companies, "Id", "Name", asset.CompanyId);
            ViewBag.ContactUserId = new SelectList(db.users, "Id", "Firstname", asset.ContactUserId);
            ViewBag.RoadId = new SelectList(db.roads, "Id", "Title", asset.RoadId);
            ViewBag.LeaseId = new SelectList(db.leases, "Id", "Name", asset.LeaseId);
            ViewBag.AssetTypeId = new SelectList(db.assettypes, "Id", "Title", asset.AssetTypeId);
            ViewBag.StatusId = new SelectList(db.assetstatuses, "Id", "Title", asset.StatusId);
            ViewBag.PaymentStatus = new SelectList(db.paymentstatuses, "Id", "Title", asset.PaymentStatusId);
            ViewBag.payment_statuses = db.paymentstatuses.ToList();
            return View(asset);
        }

        // POST: Assets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(asset asset)
        {
            try
            {
                var old_asset = db.assets.Where(i => i.Id == asset.Id).FirstOrDefault();
                //update only the change fields
                old_asset.Title = asset.Title;
                old_asset.CompanyId = asset.CompanyId;
                old_asset.StatusId = asset.StatusId;
                old_asset.PaymentStatusId = asset.PaymentStatusId;
                //old_asset.paymentstatus = asset.paymentstatus;
                old_asset.Latitude = asset.Latitude;
                old_asset.Longitude = asset.Longitude;
                old_asset.Balance = asset.Balance;
                old_asset.length = asset.length;
                old_asset.Width = asset.Width;
                old_asset.RoadId = asset.RoadId;
                old_asset.LocationDescription = asset.LocationDescription;
                db.SaveChanges();
                TempData["tmsg"] = "Saved";
                TempData["type"] = "success";

                Task.Run(() => {
                    var db = new HCityEntities();
                    var company = db.companies.Where(i => i.Id == old_asset.CompanyId).FirstOrDefault();
                    var client = new HttpClient();
                    var email = new mEmailMessage();
                    email.to_email = company.Email;
                    email.message = $@"Dear valued customer. Your asset {asset.Title}
                                has been modified by the system. Log into your account to review the changes";
                    var json = JsonConvert.SerializeObject(email);
                    var scontent = new StringContent(json, Encoding.UTF8, "application/json");
                    var res = client.PostAsync($"{Globals.notification_service_base_url}/Notification/SendEmail", scontent).Result;
                    Globals.log_data_to_file("hassan","");
                    db.Dispose();
                });
            }
            catch(Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }
            return RedirectToAction("Edit", new { id = asset.Id });
        }


        // GET: Assets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            asset asset = db.assets.Find(id);
            if (asset == null)
            {
                return HttpNotFound();
            }
            return View(asset);
        }

        // POST: Assets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            asset asset = db.assets.Find(id);
            db.assets.Remove(asset);
            db.SaveChanges();
            Task.Run(() => {
                var company = db.companies.Where(i => i.Id == asset.CompanyId).FirstOrDefault();
                var client = new HttpClient();
                var email = new mEmailMessage();
                email.to_email = company.Email;
                email.message = $@"Dear valued customer. Your asset {asset.Title}
                                has been Removed from your account. Log into your account to review the changes";

                var json = JsonConvert.SerializeObject(email);
                var scontent = new StringContent(json, Encoding.UTF8, "application/json");
                var res = client.PostAsync($"{Globals.notification_service_base_url}/Notification/SendEmail", scontent).Result;
            });
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
