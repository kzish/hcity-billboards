﻿using HcityAssets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HcityAssets.Controllers
{
    [Authorize]

    public class HomeController : Controller
    {
        private HCityEntities db = new HCityEntities();

        public ActionResult Index()
        {
            
            return View(db.assets.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "";

            return View();
        }
    }
}