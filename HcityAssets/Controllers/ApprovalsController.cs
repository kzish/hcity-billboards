﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class ApprovalsController : Controller
    {
        private HCityEntities db = new HCityEntities();

        // GET: Approvals
       
        public ActionResult Index()
        {
            var approvals = db.approvals.Include(a => a.asset).Include(a => a.user).Include(a => a.assetstatus);
            return View(approvals.ToList());
        }

        // GET: Approvals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            approval approval = db.approvals.Find(id);
            if (approval == null)
            {
                return HttpNotFound();
            }
            return View(approval);
        }

        // GET: Approvals/Create
        public ActionResult Create()
        {
            ViewBag.AssetsId = new SelectList(db.assets, "Id", "Title");
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname");
            ViewBag.AssetStatusId = new SelectList(db.assetstatuses, "Id", "Title");
            return View();
        }

        // POST: Approvals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AssetsId,UserId,ApprovalDate,IsApproved,AssetStatusId")] approval approval)
        {
            if (ModelState.IsValid)
            {
                db.approvals.Add(approval);
                db.SaveChanges();
              
                //update asset
                var asset = db.assets.Find(approval.AssetsId);
                var approvalcount = db.approvalstages.Where(a => a.AssetStatusId == asset.StatusId).Count();
                var totalapprovalcount = db.approvals.Where(a => a.AssetsId == asset.Id).Count();
                if (approvalcount == totalapprovalcount)
                {
                    if (asset.StatusId == 1 && asset.AssetTypeId == 3)  //dev permit for Trench
                        asset.StatusId = 2;                             //now at Trench permit level
                    else if (asset.StatusId == 1)                       //dev permit for Billboard and tower
                        asset.StatusId = 3;                             // Active

                    if (asset.StatusId == 2 && asset.AssetTypeId == 3)   //Trench permit for Trench
                        asset.StatusId = 3;                             //Active

                    db.Entry(asset).State = EntityState.Modified;
                    db.SaveChanges();
                }

                 return RedirectToAction("Index");
            }

            ViewBag.AssetsId = new SelectList(db.assets, "Id", "Title", approval.AssetsId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", approval.UserId);
            ViewBag.AssetStatusId = new SelectList(db.assetstatuses, "Id", "Title", approval.AssetStatusId);
            return View(approval);
        }

        // GET: Approvals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            approval approval = db.approvals.Find(id);
            if (approval == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssetsId = new SelectList(db.assets, "Id", "Title", approval.AssetsId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", approval.UserId);
            ViewBag.AssetStatusId = new SelectList(db.assetstatuses, "Id", "Title", approval.AssetStatusId);
            return View(approval);
        }

        // POST: Approvals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AssetsId,UserId,ApprovalDate,IsApproved,AssetStatusId")] approval approval)
        {
            if (ModelState.IsValid)
            {
                db.Entry(approval).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AssetsId = new SelectList(db.assets, "Id", "Title", approval.AssetsId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", approval.UserId);
            ViewBag.AssetStatusId = new SelectList(db.assetstatuses, "Id", "Title", approval.AssetStatusId);
            return View(approval);
        }

        // GET: Approvals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            approval approval = db.approvals.Find(id);
            if (approval == null)
            {
                return HttpNotFound();
            }
            return View(approval);
        }

        // POST: Approvals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            approval approval = db.approvals.Find(id);
            db.approvals.Remove(approval);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
