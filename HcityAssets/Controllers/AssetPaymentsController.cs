﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class AssetPaymentsController : Controller
    {
        private HCityEntities db = new HCityEntities();

        // GET: AssetPayments
        
        public ActionResult Index( string AssetId , string FeesId, string UserId, string byoverdue , string CompanyId , string AssetTypeId , bool? FeesCheckbox, bool? UserCheckbox , bool? AssetCheckbox, bool? AssetTypeCheckbox , bool? CompanyCheckbox, bool? OverdueCheckbox)
        {
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname");
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title");
            ViewBag.AssetId = new SelectList(db.assets, "Id", "Title");
            ViewBag.CompanyId = new SelectList( db.companies, "Id", "Name");
            ViewBag.AssetTypeId = new SelectList(db.assettypes, "Id", "Title");

            IEnumerable<assetpayment> assetPayments = null;
            assetPayments = db.assetpayments.Include(a => a.user).Include(a => a.fee).Include(a => a.asset).ToList();
            if ( AssetCheckbox ==true && !string.IsNullOrEmpty(AssetId))
            {
                 assetPayments = assetPayments.Where(a => a.AssetId == int.Parse(AssetId));
            }

             if ( FeesCheckbox ==true  && !string.IsNullOrEmpty(FeesId))
            {
                 assetPayments = assetPayments.Where(a => a.FeesId == int.Parse(FeesId));
            }

            if ( UserCheckbox == true && !string.IsNullOrEmpty(UserId))
            {
                assetPayments = assetPayments.Where(a => a.UserId == int.Parse(UserId));
            }

            if ( AssetTypeCheckbox == true &&  !string.IsNullOrEmpty(AssetTypeId))
            {
                assetPayments = assetPayments.Where(a => a.asset.AssetTypeId == int.Parse(AssetTypeId));
            }

            if ( CompanyCheckbox == true && !string.IsNullOrEmpty(CompanyId))
            {
                assetPayments = assetPayments.Where(a => a.asset.CompanyId == int.Parse(CompanyId));
            }

            if ( OverdueCheckbox == true && !string.IsNullOrEmpty(byoverdue))
            {
                switch(byoverdue)
                {
                    case "0":
                        break;
                    case "1":
                        assetPayments = assetPayments.Where(a => a.asset.PaymentStatusId == 1);
                        break;
                    case "2":
                        assetPayments = assetPayments.Where(a => a.asset.PaymentStatusId == 2);
                        break;
                    case "3":
                        assetPayments = assetPayments.Where(a => a.asset.PaymentStatusId == 3);
                        break;
                    case "4":
                        assetPayments = assetPayments.Where(a => a.asset.PaymentStatusId == 4);
                        break;
                    case "5":
                        assetPayments = assetPayments.Where(a => a.asset.PaymentStatusId == 5);
                        break;
                    case "6":
                        assetPayments = assetPayments.Where(a => a.asset.PaymentStatusId == 6);
                        break;
                }
               
            }
            
            return View(assetPayments.ToList());
        }

        // GET: AssetPayments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            assetpayment assetPayment = db.assetpayments.Find(id);
            if (assetPayment == null)
            {
                return HttpNotFound();
            }
            return View(assetPayment);
        }

        // GET: AssetPayments/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname");
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title");
            ViewBag.AssetId = new SelectList(db.assets, "Id", "Title");
            return View();
        }

        // POST: AssetPayments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PaymentDate,UserId,FeesId,AssetId,Amount,IsPayment,PaymentType,TransactionId")] assetpayment assetPayment)
        {
            if (ModelState.IsValid)
            {
                db.assetpayments.Add(assetPayment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", assetPayment.UserId);
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title", assetPayment.FeesId);
            ViewBag.AssetId = new SelectList(db.assets, "Id", "Title", assetPayment.AssetId);
            return View(assetPayment);
        }

        // GET: AssetPayments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            assetpayment assetPayment = db.assetpayments.Find(id);
            if (assetPayment == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", assetPayment.UserId);
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title", assetPayment.FeesId);
            ViewBag.AssetId = new SelectList(db.assets, "Id", "Title", assetPayment.AssetId);
            return View(assetPayment);
        }

        // POST: AssetPayments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PaymentDate,UserId,FeesId,AssetId,Amount,IsPayment,PaymentType,TransactionId")] assetpayment assetPayment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assetPayment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", assetPayment.UserId);
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title", assetPayment.FeesId);
            ViewBag.AssetId = new SelectList(db.assets, "Id", "Title", assetPayment.AssetId);
            return View(assetPayment);
        }

        // GET: AssetPayments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            assetpayment assetPayment = db.assetpayments.Find(id);
            if (assetPayment == null)
            {
                return HttpNotFound();
            }
            return View(assetPayment);
        }

        // POST: AssetPayments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            assetpayment assetPayment = db.assetpayments.Find(id);
            db.assetpayments.Remove(assetPayment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
