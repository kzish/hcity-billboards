﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;
using System.Linq;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class companiesController : Controller
    {
        private HCityEntities db = new HCityEntities();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public companiesController()
        {
        }

        public companiesController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: companies
        public ActionResult Index()
        {
            return View(db.companies.ToList());
        }

        // GET: companies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company company = db.companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // GET: companies/Create
        public ActionResult Create()
        {
            return View(new company());
        }

        // POST: companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //create the company wc is the user account for that company
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(string companyname, string tel, string email, string password)
        {
            //create  a company and the user for that company
            var user = new ApplicationUser()
            {
                Email = email,
                PasswordHash = password,
                PhoneNumber = tel,
                UserName = email
            };

            //create the user
            var result = await UserManager.CreateAsync(user, password);

            if (result.Succeeded)
            {
                //create company associated with the user
                var company = new company()
                {
                    Name = companyname,
                    Email = email,
                    Phone=tel
                };

                db.companies.Add(company);
                db.SaveChanges();
                TempData["tmsg"] = "Created Company";
                TempData["type"] = "success";

            }
            else
            {
                TempData["tmsg"] = "Error occured, Email exists";
                TempData["type"] = "error";
            }


            return RedirectToAction("Index");
        }

        // GET: companies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company company = db.companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(company company)
        {
            if (ModelState.IsValid)
            {
                db.Entry(company).State = EntityState.Modified;
                //change the email of the user to match the email of the company
                var company_user = db.aspnetusers.Where(i => i.Email == company.Email).First();
                company_user.Email = company.Email;
                company_user.PhoneNumber = company.Phone;
                company_user.UserName = company.Email;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(company);
        }

        // GET: companies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            company company = db.companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            company company = db.companies.Find(id);
            db.companies.Remove(company);
            //delete also the user of that company
            var company_user = db.aspnetusers.Where(i => i.Email == company.Email).First();
            db.aspnetusers.Remove(company_user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
