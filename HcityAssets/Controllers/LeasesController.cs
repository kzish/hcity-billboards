﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;
using Newtonsoft.Json;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class LeasesController : Controller
    {
        private HCityEntities db = new HCityEntities();

        // GET: Leases
      
        public ActionResult Index()
        {
            var leases = db.leases.Include(l => l.leasetype).Include(l => l.asset).Include(l => l.leasestatus);
            return View(leases.ToList());
        }

        // GET: Leases/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lease lease = db.leases.Find(id);
            if (lease == null)
            {
                return HttpNotFound();
            }
            return View(lease);
        }

        // GET: Leases/Create
        public ActionResult Create()
        {
            ViewBag.StatusId = new SelectList(db.leasestatuses, "Id", "Title");
            ViewBag.AssetId = new SelectList(db.assets, "Id", "Title");
            ViewBag.LeaseTypeId = new SelectList(db.leasetypes, "Id", "Title");
            //only grab assets which dont already have a lease
            ViewBag.assets = db.assets.Include(c => c.company).Where(i=>i.LeaseId==null).ToList();
            return View();
        }

        // POST: Leases/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(lease lease)
        {
            if (ModelState.IsValid)
            {
                //get the asset with this lease
                var asset = db.assets.Where(i => i.Id == lease.AssetId).First();
                var company = db.companies.Where(i => i.Id == asset.CompanyId).FirstOrDefault();
                var lease_id = db.leases.Add(lease).Id;
                asset.LeaseId = lease.Id;
                db.SaveChanges();

                Task.Run(() => {
                    //send email to the client
                    var client = new HttpClient();
                    var email = new mEmailMessage();
                    email.to_email = company.Email;
                    email.message = $@"Dear valued customer. A lease has just been created for your asset {asset.Title}.
                                Log in to review the lease.";

                    var json = JsonConvert.SerializeObject(email);
                    var scontent = new StringContent(json, Encoding.UTF8, "application/json");
                    var res = client.PostAsync($"{Globals.notification_service_base_url}/Notification/SendEmail", scontent).Result;

                });

                return RedirectToAction("Index");
            }
            ViewBag.StatusId = new SelectList(db.leasestatuses, "Id", "Title", lease.StatusId);
            ViewBag.AssetId = new SelectList(db.assets, "Id", "Title", lease.AssetId);
            ViewBag.LeaseTypeId = new SelectList(db.leasetypes, "Id", "Title", lease.LeaseTypeId);
            return View(lease);
        }

        // GET: Leases/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lease lease = db.leases.Find(id);
            if (lease == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssetId = new SelectList(db.assets, "Id", "Title", lease.AssetId);
            ViewBag.LeaseTypeId = new SelectList(db.leasetypes, "Id", "Title", lease.LeaseTypeId);
            ViewBag.StatusId = new SelectList(db.leasestatuses, "Id", "Title", lease.StatusId);
            //only grab all assets but make readonly
            ViewBag.asset = db.assets.Find(lease.AssetId);
            return View(lease);
        }

        // POST: Leases/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(lease lease)
        {
            if (ModelState.IsValid)
            {
                var asset = db.assets.Where(i => i.Id == lease.AssetId).First();
                var company = db.companies.Where(i => i.Id == asset.CompanyId).FirstOrDefault();

                db.Entry(lease).State = EntityState.Modified;
                db.SaveChanges();
                Task.Run(() => {
                    //send email to the client
                    var client = new HttpClient();
                    var email = new mEmailMessage();
                    email.to_email = company.Email;
                    email.message = $@"Dear valued customer. A lease has just been modified for your asset {asset.Title}.
                                Log in to review the lease.";
                    var json = JsonConvert.SerializeObject(email);
                    var scontent = new StringContent(json, Encoding.UTF8, "application/json");
                    var res = client.PostAsync($"{Globals.notification_service_base_url}/Notification/SendEmail", scontent).Result;

                });
                return RedirectToAction("Index");
            }
            ViewBag.AssetId = new SelectList(db.assets, "Id", "Title", lease.AssetId);
            ViewBag.LeaseTypeId = new SelectList(db.leasetypes, "Id", "Title", lease.LeaseTypeId);
            ViewBag.StatusId = new SelectList(db.leasestatuses, "Id", "Title", lease.StatusId);
            return View(lease);
        }

        // GET: Leases/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            lease lease = db.leases.Find(id);
            if (lease == null)
            {
                return HttpNotFound();
            }
            return View(lease);
        }

        // POST: Leases/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //unset the lease id
            var asset = db.assets.Where(i => i.LeaseId == id).FirstOrDefault();
            if(asset!=null)asset.LeaseId = null;
            lease lease = db.leases.Find(id);
            db.leases.Remove(lease);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
