﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class RoadsController : Controller
    {
        private HCityEntities db = new HCityEntities();

        // GET: Roads
        public ActionResult Index()
        {
            return View(db.roads.ToList());
        }

        // GET: Roads/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            road road = db.roads.Find(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // GET: Roads/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Roads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title")] road road)
        {
            if (ModelState.IsValid)
            {
                db.roads.Add(road);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(road);
        }

        // GET: Roads/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            road road = db.roads.Find(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // POST: Roads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title")] road road)
        {
            if (ModelState.IsValid)
            {
                db.Entry(road).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(road);
        }

        // GET: Roads/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            road road = db.roads.Find(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // POST: Roads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            road road = db.roads.Find(id);
            db.roads.Remove(road);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
