﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;
using Newtonsoft.Json;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class NotificationsController : Controller
    {
        private HCityEntities db = new HCityEntities();

        // GET: Notifications
        public ActionResult Index(string date_from, string date_to)
        {
            ViewBag.Title = "Notifications";
            var notifications = db.notifications.Include(n => n.user);
            if (!string.IsNullOrEmpty(date_from))
            {
                var date_from_ = DateTime.Parse(date_from);
                notifications = notifications.Where(i => i.Date >= date_from_);
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                var date_to_ = DateTime.Parse(date_to);
                notifications = notifications.Where(i => i.Date <= date_to_);
            }
           
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;

            var notifications_ = notifications.OrderByDescending(i => i.Date).ToList();
            return View(notifications_);
        }

        // GET: Notifications/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.Title = "View Notification";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            notification notification = db.notifications.Find(id);
            if (notification == null)
            {
                return HttpNotFound();
            }
            return View(notification);
        }

        // GET: Notifications/Create
        public ActionResult Create()
        {
            ViewBag.companies = db.companies.ToList();
            ViewBag.Title = "Create Notification";
            return View();
        }

        // POST: Notifications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(notification notification)
        {
            try
            {
                TempData["tmsg"] = "Notification saved";
                TempData["type"] = "success";

                Task.Run(() => {
                    var db = new HCityEntities();
                    var company = db.companies.Where(i => i.Id == notification.CompanyId).FirstOrDefault();
                    //send email to the client
                    var client = new HttpClient();
                    var email = new mEmailMessage();
                    email.to_email = company.Email;
                    email.message = notification.Message;

                    var json = JsonConvert.SerializeObject(email);
                    var scontent = new StringContent(json, Encoding.UTF8, "application/json");
                    var res = client.PostAsync($"{Globals.notification_service_base_url}/Notification/SendEmail", scontent).Result;
                    db.Dispose();
                });


            }
            catch(Exception ex)
            {
                TempData["tmsg"] = "Error";
                TempData["type"] = "error";
                TempData["ex"] = ex.Message;
            }

            return RedirectToAction("Index");

        }

        // GET: Notifications/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Title = "Edit Notification";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            notification notification = db.notifications.Find(id);
            if (notification == null)
            {
                return HttpNotFound();
            }
            ViewBag.companies = db.companies.ToList();
            return View(notification);
        }

        //method not used
        [HttpPost]
        [ValidateAntiForgeryToken]
        private ActionResult Edit(notification notification)
        {
            //to edit you must first delete the current one, the service will recreate the message
            //var old_notification = db.notifications.Where(i=>i.CompanyId==)
            //db.notifications.Remove(notification);
            if (ModelState.IsValid)
            {
                 Task.Run(() => {
                    var db = new HCityEntities();
                    var company = db.companies.Where(i => i.Id == notification.CompanyId).FirstOrDefault();
                    //send email to the client
                    var client = new HttpClient();
                    var email = new mEmailMessage();
                    email.to_email = company.Email;
                    email.message = notification.Message;

                    var json = JsonConvert.SerializeObject(email);
                    var scontent = new StringContent(json, Encoding.UTF8, "application/json");
                    var res = client.PostAsync($"{Globals.notification_service_base_url}/Notification/SendEmail", scontent).Result;
                    db.Dispose();
                });

                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", notification.UserId);
            return View(notification);
        }

        // GET: Notifications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            notification notification = db.notifications.Find(id);
            if (notification == null)
            {
                return HttpNotFound();
            }
            return View(notification);
        }

        // POST: Notifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            notification notification = db.notifications.Find(id);
            db.notifications.Remove(notification);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
