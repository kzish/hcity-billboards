﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;

namespace HcityAssets.Controllers
{
    [RoutePrefix("Admin")]
    public class AdminController : Controller
    {
        private HCityEntities db = new HCityEntities();
        
        [Route("Emails")]
        public ActionResult Emails()
        {
            ViewBag.Title = "Emails";
            var emails = db.admin_emails.ToList();
            ViewBag.emails = emails;
            return View();
        }

        [Route("AddEmail")]
        [HttpPost]
        public ActionResult AddEmail(string email)
        {
            var email_ = new admin_emails();
            email_.email = email;
            db.admin_emails.Add(email_);
            db.SaveChanges();
            return RedirectToAction("Emails");
        }

        [Route("RemoveEmail/{id}")]
        public ActionResult RemoveEmail(int id)
        {
            var email = db.admin_emails.Find(id);
            db.admin_emails.Remove(email);
            db.SaveChanges();
            return RedirectToAction("Emails");
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}