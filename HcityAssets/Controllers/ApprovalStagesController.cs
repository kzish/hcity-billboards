﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class ApprovalStagesController : Controller
    {
        private HCityEntities db = new HCityEntities();

        // GET: ApprovalStages
        
        public ActionResult Index()
        {
            var approvalStages = db.approvalstages.Include(a => a.role);
            return View(approvalStages.ToList());
        }

        // GET: ApprovalStages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            approvalstage approvalStage = db.approvalstages.Find(id);
            if (approvalStage == null)
            {
                return HttpNotFound();
            }
            return View(approvalStage);
        }

        // GET: ApprovalStages/Create
        public ActionResult Create()
        {
            ViewBag.RoleId = new SelectList(db.roles, "Id", "Title");
            return View();
        }

        // POST: ApprovalStages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AssetStatus,RoleId")] approvalstage approvalStage)
        {
            if (ModelState.IsValid)
            {
                db.approvalstages.Add(approvalStage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RoleId = new SelectList(db.roles, "Id", "Title", approvalStage.RoleId);
            return View(approvalStage);
        }

        // GET: ApprovalStages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            approvalstage approvalStage = db.approvalstages.Find(id);
            if (approvalStage == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoleId = new SelectList(db.roles, "Id", "Title", approvalStage.RoleId);
            return View(approvalStage);
        }

        // POST: ApprovalStages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AssetStatusId,RoleId")] approvalstage approvalStage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(approvalStage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(db.roles, "Id", "Title", approvalStage.RoleId);
            return View(approvalStage);
        }

        // GET: ApprovalStages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            approvalstage approvalStage = db.approvalstages.Find(id);
            if (approvalStage == null)
            {
                return HttpNotFound();
            }
            return View(approvalStage);
        }

        // POST: ApprovalStages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            approvalstage approvalStage = db.approvalstages.Find(id);
            db.approvalstages.Remove(approvalStage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
