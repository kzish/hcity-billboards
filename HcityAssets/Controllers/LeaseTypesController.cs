﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class LeaseTypesController : Controller
    {
        private HCityEntities db = new HCityEntities();

        // GET: LeaseTypes
       
        public ActionResult Index()
        {
            return View(db.leasetypes.ToList());
        }

        // GET: LeaseTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            leasetype leaseType = db.leasetypes.Find(id);
            if (leaseType == null)
            {
                return HttpNotFound();
            }
            return View(leaseType);
        }

        // GET: LeaseTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LeaseTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Description,Amount,PenaltyFees,Length,Width,Distance,Years")] leasetype leaseType)
        {
            if (ModelState.IsValid)
            {
                db.leasetypes.Add(leaseType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(leaseType);
        }

        // GET: LeaseTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            leasetype leaseType = db.leasetypes.Find(id);
            if (leaseType == null)
            {
                return HttpNotFound();
            }
            return View(leaseType);
        }

        // POST: LeaseTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,Amount,PenaltyFees,Length,Width,Distance,Years")] leasetype leaseType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(leaseType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(leaseType);
        }

        // GET: LeaseTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            leasetype leaseType = db.leasetypes.Find(id);
            if (leaseType == null)
            {
                return HttpNotFound();
            }
            return View(leaseType);
        }

        // POST: LeaseTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            leasetype leaseType = db.leasetypes.Find(id);
            db.leasetypes.Remove(leaseType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
