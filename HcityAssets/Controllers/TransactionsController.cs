﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HcityAssets.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace HcityAssets.Controllers
{
    [Authorize]
    public class TransactionsController : Controller
    {
        private HCityEntities db = new HCityEntities();

        // GET: Transactions
        public ActionResult Index(string date_from,string date_to,int fee_id=-1)
        {
            var transactions = db.transactions.Include(t => t.fee).Include(t => t.user).Include(t => t.user1);
            if(!string.IsNullOrEmpty(date_from))
            {
                var date_from_ = DateTime.Parse(date_from);
                transactions = transactions.Where(i => i.PaymentDate >= date_from_);
            }
            if (!string.IsNullOrEmpty(date_to))
            {
                var date_to_ = DateTime.Parse(date_to);
                transactions = transactions.Where(i => i.PaymentDate <= date_to_);
            }
            if(fee_id!=-1)
            {
                transactions = transactions.Where(i => i.FeesId == fee_id);
            }
            ViewBag.date_from = date_from;
            ViewBag.date_to = date_to;
            ViewBag.fees = db.fees.ToList();
            ViewBag.fee_id = fee_id;
            
            var payments = transactions.OrderByDescending(i=>i.PaymentDate).ToList();
            return View(payments);
        }

        // GET: Transactions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transaction transaction = db.transactions.Find(id);
            var asset = db.assets.Find(transaction.AssetId);
            var company = db.companies.Find(asset.CompanyId);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            ViewBag.payment = transaction;
            ViewBag.company = company;
            return View();
        }

        // GET: Transactions/Create
        public ActionResult Create()
        {
            ViewBag.Assets = db.assets.Where(a => a.user.Id == 1).ToList();
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title");
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname");
            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(transaction transaction, HttpPostedFileBase file , int[] Assets , string[] AssetAmount)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (file.ContentLength > 0)
                    {
                         string filetype = Path.GetFileName(file.ContentType);
                        string _FileName = DateTime.Now.ToString("ddMMyyyy") + "-" + transaction.UserId + "-" + transaction.FeesId + "-" + transaction.Amount + "-" + transaction.PaymentMethod + "-" + "."+ filetype;
                        string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                        transaction.Filename = _FileName;
                    }
                    
                    ViewBag.Message = "File Uploaded Successfully!!";
                  //  return View();
                }
                catch ( Exception ex)
                {
                    ViewBag.Message = "File upload failed!!";
                  //  return View();
                }

                db.transactions.Add(transaction);
                db.SaveChanges();

                //Reduce Company balance
                transaction.user = db.users.Find(transaction.UserId);
                var company = db.companies.Find(transaction.user.CompanyId);
                db.Entry(company).State = EntityState.Modified;
                db.SaveChanges();

                //reduce assets balances
                var i = 0;
                foreach (var item in Assets)
                {
                    var asset = db.assets.Find(item);
                    asset.Balance = asset.Balance - Convert.ToDecimal(AssetAmount[i]);
                    db.Entry(asset).State = EntityState.Modified;
                    db.SaveChanges();
                    i++;
                }
                
                //Save assetpayments
                 i = 0;
                var assetpayment = new assetpayment();
                foreach (var item in Assets)
                {
                    assetpayment = new assetpayment { TransactionId= transaction.Id , AssetId = item , Amount = Convert.ToDecimal(AssetAmount[i]),  PaymentMethod = transaction.PaymentMethod , IsPayment = true , UserId= transaction.UserId , FeesId = transaction.FeesId };
                    db.assetpayments.Add(assetpayment);
                    db.SaveChanges();
                    i++;
                }
                
                return RedirectToAction("Index");
            }
            ViewBag.Assets = db.assets.Where(a => a.user.Id == 1).ToList();
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title", transaction.FeesId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", transaction.UserId);
            return View(transaction);
        }

        // GET: Transactions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transaction transaction = db.transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            ViewBag.Assets = db.assets.Where(a => a.user.Id == 1).ToList();
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title", transaction.FeesId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", transaction.UserId);
            return View(transaction);
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(transaction transaction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transaction).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Assets = db.assets.Where(a => a.user.Id == 1).ToList();
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title", transaction.FeesId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", transaction.UserId);
            return View(transaction);
        }


       

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        private ActionResult Approve_([Bind(Include = "Id,PaymentDate,UserId,FeesId,AssetId,Amount,PaymentMethod,PaymentType,IsApproved,ApprovedDate,ApprovedByUserId")] transaction transaction)
        {
            if (ModelState.IsValid)
            {
                if (transaction.IsApproved)
                {
                    string currentUserId = User.Identity.GetUserId();
                    var currentUser = db.users.Where(a => a.IdentityUserId == currentUserId).FirstOrDefault();
                    transaction.ApprovedByUserId = currentUser.Id;
                    transaction.ApprovedDate = DateTime.Now;
                    transaction.IsPayment = true;

                    db.Entry(transaction).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Assets = db.assets.Where(a => a.user.Id == 1).ToList();
            ViewBag.FeesId = new SelectList(db.fees, "Id", "Title", transaction.FeesId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Firstname", transaction.UserId);
            ViewBag.ApprovedByUserId = new SelectList(db.users, "Id", "Firstname", transaction.ApprovedByUserId);
            return View(transaction);
        }

        // GET: Transactions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transaction transaction = db.transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            transaction transaction = db.transactions.Find(id);
            db.transactions.Remove(transaction);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [Route("Approve/{id}")]
        public ActionResult Approve(int id)
        {
            //approve the transaction

            var payment = db.transactions.Where(i => i.Id == id).FirstOrDefault();
            payment.IsApproved = true;
            payment.isRejected = false;
            payment.ApprovedDate = DateTime.Now;
            if(payment.FeesId==5)//top up asset
            {
                var asset = db.assets.Where(i => i.Id == payment.AssetId).First();
                asset.Balance = (decimal)asset.Balance + (decimal)payment.Amount;
            }
            db.SaveChanges();
            TempData["tmsg"] = "Saved";
            TempData["type"] = "Success";

            //send email to client
            Task.Run(() => {
                var db = new HCityEntities();
                var asset = db.assets.Where(i => i.Id == payment.AssetId).First();
                var company = db.companies.Where(i => i.Id == asset.CompanyId).FirstOrDefault();
                var client = new HttpClient();
                var email = new mEmailMessage();
                email.to_email = company.Email;
                email.message = $@"Dear valued customer. Your payment for the asset {asset.Title}
                                has been approved.";
                var json = JsonConvert.SerializeObject(email);
                var scontent = new StringContent(json, Encoding.UTF8, "application/json");
                var res = client.PostAsync($"{Globals.notification_service_base_url}/Notification/SendEmail", scontent).Result;
                db.Dispose();
            });


            return RedirectToAction("/Details/"+id);

        }

        [Route("Reject/{id}")]
        public ActionResult Reject(int id)
        {
            //approve the transaction

            var payment = db.transactions.Where(i => i.Id == id).FirstOrDefault();
            payment.IsApproved = false;
            payment.isRejected = true;
            payment.ApprovedDate = DateTime.Now;
            db.SaveChanges();
            TempData["tmsg"] = "Saved";
            TempData["type"] = "success";
            //send email to client
            Task.Run(() => {
                var db = new HCityEntities();
                var asset = db.assets.Where(i => i.Id == payment.AssetId).First();
                var company = db.companies.Where(i => i.Id == asset.CompanyId).FirstOrDefault();
                var client = new HttpClient();
                var email = new mEmailMessage();
                email.to_email = company.Email;
                email.message = $@"Dear valued customer. Your payment for the asset {asset.Title}
                                has been rejected. Contact your account manager";
                var json = JsonConvert.SerializeObject(email);
                var scontent = new StringContent(json, Encoding.UTF8, "application/json");
                var res = client.PostAsync($"{Globals.notification_service_base_url}/Notification/SendEmail", scontent).Result;
                db.Dispose();
            });

            return RedirectToAction("/Details/" + id);

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
