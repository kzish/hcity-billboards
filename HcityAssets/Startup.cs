﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HcityAssets.Startup))]
namespace HcityAssets
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
