﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace HcityAssets.Models
{
    /// <summary>
    /// global helper class
    /// </summary>
    public class Globals
    {
        private static string google_maps_api_key = "AIzaSyBQrtU5y53d68uzWEsykk62vhtd2UDwTZY";
        //base address to the client
        public static string client_base_address = "https://localhost:44317/";

        public static string log_path = @"c:\rubiem\logs\hcity_logs.txt";
        public static string notification_service_base_url = "https://localhost:4002";


        public static void log_data_to_file(string source, string data)
        {
            System.IO.File.AppendAllText(log_path, $"source:{source}   Data:{data}" + Environment.NewLine);
        }


        public static string get_address_from_geolocation(double latitude, double longitude)
        {
            try
            {
                WebClient wc = new WebClient();
                var data = wc.DownloadString($@"
                    https://maps.googleapis.com/maps/api/geocode/json?latlng=
                    {latitude},{longitude}&key={google_maps_api_key}"
                    );
                dynamic json = JsonConvert.DeserializeObject(data);
                var results = json.results[0];
                var address = results.formatted_address;
                return address;
            }
            catch (Exception ex)
            {
                return "unkown location";
            }
        }


        /// <summary>
        /// returns a hash of the input
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string get_hash(string input)
        {
            var hashedBytes = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(input));
            return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
        }

        public static double compute_company_balance(int company_id)
        {
            var db = new HCityEntities();
            var balance = (double)db.assets.Where(i => i.CompanyId == company_id).ToList().Sum(i => i.Balance);
            var used = (double)db.assets.Where(i => i.CompanyId == company_id ).ToList().Sum(i => i.Used);
            db.Dispose();
            var remaining=(balance-used);
            return remaining;
        }

        public static lease get_lease_from_id(int id)
        {
            var db = new HCityEntities();
            var lease = db.leases.Find(id);
            db.Dispose();
            return lease??new lease() { Name=""};
        }

        public static string get_company_name_from_email(string email)
        {
            var db = new HCityEntities();
            var company = db.companies.Where(i => i.Email == email).FirstOrDefault() ?? new company(); ;
            db.Dispose();
            return company.Name;
        }
        public static int get_company_id_from_email(string email)
        {
            var db = new HCityEntities();
            var company = db.companies.Where(i => i.Email == email).FirstOrDefault()??new company();
            db.Dispose();
            return company.Id;
        }

        public static string get_road_name(int? id)
        {
            var db = new HCityEntities();
            var item = db.roads.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_payment_status_name(int? id)
        {
            var db = new HCityEntities();
            var item = db.paymentstatuses.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_asset_status_name(int? id)
        {
            var db = new HCityEntities();
            var item = db.assetstatuses.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_fees_name(int? id)
        {
            var db = new HCityEntities();
            var item = db.fees.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_asset_name(int? id)
        {
            var db = new HCityEntities();
            var item = db.assets.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_asset_type_name(int? id)
        {
            var db = new HCityEntities();
            var item = db.assettypes.Find(id);
            db.Dispose();
            return item?.Title;
        }

        public static string get_company_name(int? id)
        {
            var db = new HCityEntities();
            var item = db.companies.Find(id);
            db.Dispose();
            return item?.Name;
        }

    }
}