﻿using System.Web.Mvc;
using System.Web.Routing;

namespace HcityAssets
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "api",
               url: "api/{action}/",
              defaults: new { controller = "api" }
           );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
